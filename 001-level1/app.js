const http = require('http')

const server = http.createServer((req, res) => {
    console.log('server is running');
    console.log(req.url, req.method, req.headers)
    process.exit()
});

server.listen(5000);