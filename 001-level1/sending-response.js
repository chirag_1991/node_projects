const http = require('http')

const server = http.createServer((req, res) => {
    console.log('Server running');
    res.setHeader('content-type', 'text/html');
    res.write('<h1>');
    res.write('Node Js server is running');
    res.write('</h1>');
    res.end();
});

server.listen(5000);